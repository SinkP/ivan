Для сборки development сервера:

docker build -t unknown-app/development_server_run -f Dockerfile-dev.dockerfile .

Для запуска development сервера:

docker run -it --rm -p 3000:3000 unknown-app/development_server_run

--------------------------


Для сборки production сервера:

docker build -t unknown-app/production_server_run -f Dockerfile-production.dockerfile .

Для запуска production сервера:

docker run -it --rm -p 10000:3000 unknown-app/production_server_run

---------------------------

Порт production сервера: 10000, а development: 3000
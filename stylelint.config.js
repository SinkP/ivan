module.exports = {
  extends: ['stylelint-config-standard', 'stylelint-config-prettier'],
  // add your custom config here
  // https://stylelint.io/user-guide/configuration
  rules: {
    'selector-pseudo-element-no-unknown': null,
    'selector-pseudo-element-allowed-list': ['v-deep', 'before', 'after'],
    'at-rule-no-unknown': null,
    'scss/at-rule-no-unknown': true,
  },
}

FROM node:lts-alpine3.13 AS development_server_run

LABEL Author="Kipran/and.buhradze@gmail.com"


RUN mkdir /app

WORKDIR /app

COPY ./package*.json ./

RUN yarn install

COPY ./ ./







EXPOSE 3000:3000/tcp

CMD ["yarn", "dev"]
FROM node:lts-alpine3.13 AS production_server_run

LABEL Author="Kipran/and.buhradze@gmail.com"


RUN mkdir /app

WORKDIR /app

COPY ./package*.json ./

RUN yarn install

COPY ./ ./


RUN yarn generate
RUN yarn build

EXPOSE 10000:3000

CMD ["yarn","start"]